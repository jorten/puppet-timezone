require 'spec_helper_acceptance'

describe 'timezone class' do
  let(:manifest) do
    <<-EOS
      class { 'timezone':
        timezone   => 'Etc/UTC',
        rtc_is_utc => true,
      }
    EOS
  end

  tzpkg =
    case fact('osfamily')
    when 'Suse'
      'timezone'
    else
      'tzdata'
    end

  it 'applies with no errors' do
    expect(apply_manifest(manifest, catch_failures: true).exit_code).to eq 2
  end

  it 'applies a second time without changes' do
    expect(apply_manifest(manifest, catch_failures: true).exit_code).to be_zero
  end

  it 'is supposed to have the correct timezone set' do
    tz_result = shell("/usr/bin/timedatectl status | grep 'Time zone:' | grep -q 'Etc/UTC'")
    expect(tz_result.exit_code).to eq 0
  end

  it 'is supposed to have RTC set on UTC time' do
    rtc_result = shell("/usr/bin/timedatectl status | grep 'RTC in local TZ:' | grep -q no")
    expect(rtc_result.exit_code).to eq 0
  end

  describe package(tzpkg) do
    it { is_expected.to be_installed }
  end
end
