require 'spec_helper'

describe 'timezone' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          timezone:   'Etc/UTC',
          rtc_is_utc: true,
          package:    'tzdata',
          timedatectl_cmd: '/usr/bin/timedatectl set-timezone',
          timedatectl_chk: "/usr/bin/timedatectl status | grep 'Time zone:' | grep -q",
          setlocalrtc_cmd: '/usr/bin/timedatectl set-local-rtc',
          setlocalrtc_chk: "/usr/bin/timedatectl status | grep 'RTC in local TZ:' | grep -q",
        }
      end

      it { is_expected.to compile.with_all_deps }

      context 'with default params' do
        it {
          is_expected.to contain_package('tzdata').with(
            ensure: 'present',
          )
        }

        it {
          is_expected.to contain_exec('set-timezone').with(
            command: '/usr/bin/timedatectl set-timezone Etc/UTC',
            unless:  "/usr/bin/timedatectl status | grep 'Time zone:' | grep -q 'Etc/UTC'",
          )
        }

        it {
          is_expected.to contain_exec('set-local-rtc').with(
            command: '/usr/bin/timedatectl set-local-rtc 0',
            unless:  "/usr/bin/timedatectl status | grep 'RTC in local TZ:' | grep -q no",
          )
        }
      end

      context 'with rtc_is_utc set to false' do
        let(:params) { super().merge(rtc_is_utc: false) }

        it {
          is_expected.to contain_exec('set-local-rtc').with(
            command: '/usr/bin/timedatectl set-local-rtc 1',
            unless:  "/usr/bin/timedatectl status | grep 'RTC in local TZ:' | grep -q yes",
          )
        }
      end
    end
  end
end
